Rails.application.routes.draw do
  resources :rounds
  resources :invoices
  get '/invoices/:id/download', action: 'download', controller: 'invoices', defaults: { format: :csv }
  resources :categories
  devise_for :users, controllers: { registrations: 'users/registrations' }
  resources :cycles
  resources :orders
  put '/orders/:id/items', action: 'add', controller: 'orders'
  delete '/orders/:id/items/:item_id', action: 'remove', controller: 'orders'
  resources :providers
  resources :products
  put '/users/:id/make_admin', action: 'make_admin', controller: 'users'
  put '/users/:id/validate', action: 'validate', controller: 'users'
  put '/users/reset_validation', action: 'reset_validation', controller: 'users'
  resources :users
  root 'homepage#index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
