json.extract! round, :id, :start_at, :freeze_at, :end_at, :created_at, :updated_at
json.url round_url(round, format: :json)
