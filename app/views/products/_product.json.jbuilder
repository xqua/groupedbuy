json.extract! product, :id, :name, :price, :min_quantity, :provider_id, :created_at, :updated_at
json.url product_url(product, format: :json)
