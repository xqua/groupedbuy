class Product < ApplicationRecord
  resourcify

  belongs_to :provider
  belongs_to :category
  has_many :conditionings, :dependent => :destroy

  accepts_nested_attributes_for :conditionings, allow_destroy: true, reject_if: :all_blank
end
