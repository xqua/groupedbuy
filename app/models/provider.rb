class Provider < ApplicationRecord
  resourcify

  has_many :products, :dependent => :destroy
end
