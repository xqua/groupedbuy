class Order < ApplicationRecord
  resourcify

  belongs_to :user
  belongs_to :round
  has_many :order_items, :dependent => :destroy

  accepts_nested_attributes_for :order_items, allow_destroy: true, reject_if: :all_blank
end
