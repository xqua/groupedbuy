class OrderItem < ApplicationRecord
  belongs_to :order
  belongs_to :category
  belongs_to :product

  def price
    product.price * quantity
  end
end
