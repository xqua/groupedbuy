class Round < ApplicationRecord
  has_many :orders, :dependent => :destroy
  has_many :invoices, :dependent => :destroy
end
