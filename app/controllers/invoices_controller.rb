
class InvoicesController < ApplicationController
  require 'csv'

  include Utils

  before_action :authenticate_user!
  before_action :is_admin
  before_action :set_invoice, only: %i[ show edit update destroy download ]

  # GET /invoices or /invoices.json
  def index
    @invoices = Invoice.all
  end

  # GET /invoices/1 or /invoices/1.json
  def show
  end

  def download
    # csv = CSV.new(@invoice.csv)
    respond_to do |format|
      format.csv { send_data @invoice.csv, filename: "invoice-#{@invoice.id}.csv" }
    end
  end

  # GET /invoices/new
  def new
    @invoice = Invoice.new
  end

  # GET /invoices/1/edit
  def edit
  end

  # POST /invoices or /invoices.json
  def create
    @invoice = Invoice.new(invoice_params)

    @invoice.user = current_user

    total_products = {}

    @invoice.round.orders.where(submitted: true).each do |order|
      @invoice.total += order.total
      order.order_items.each do |item|
        unless total_products.key?(item.product.id)
          total_products[item.product.id] = { product_id: item.product.id,
                                              product_name: item.product.name,
                                              product_quantity: 0,
                                              product_unit: item.product.unit,
                                              conditioning_id: nil,
                                              conditioning_cat_nb: nil,
                                              conditioning_name: nil,
                                              conditioning_quantity: nil,
                                              total_cost: nil
                                            }
        end
        total_products[item.product.id][:product_quantity] += item.quantity
        selected_conditioning = item.product.conditionings.first
        if item.product.conditionings.count > 1
          item.product.conditionings.each do |conditioning|
            if total_products[item.product.id][:product_quantity] > conditioning.quantity and selected_conditioning.quantity < conditioning.quantity
              selected_conditioning = conditioning
            end
          end
        end
        total_products[item.product.id][:conditioning_id] = selected_conditioning.id
        total_products[item.product.id][:conditioning_cat_nb] = selected_conditioning.cat_nb
        total_products[item.product.id][:conditioning_name] = selected_conditioning.name
        total_products[item.product.id][:conditioning_quantity] = (total_products[item.product.id][:product_quantity] / selected_conditioning.quantity).ceil
        total_products[item.product.id][:total_cost] = selected_conditioning.price * total_products[item.product.id][:conditioning_quantity]
      end
    end

    csv_file = ""
    line = "%d,%s,%f,%s,%d,%s,%s,%f\n"

    csv_file += "product_id,product_name,product_quantity,product_unit,conditioning_id,conditioning_cat_nb,conditioning_name,conditioning_quantity\n"
    total_products.each do |key, item|
      csv_file += line % [item[:product_id],item[:product_name],item[:product_quantity],item[:product_unit],item[:conditioning_id],item[:conditioning_cat_nb],item[:conditioning_name],item[:conditioning_quantity]]
    end

    @invoice.csv = csv_file

    respond_to do |format|
      if @invoice.save
        format.html { redirect_to @invoice, notice: "Invoice was successfully created." }
        format.json { render :show, status: :created, location: @invoice }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @invoice.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /invoices/1 or /invoices/1.json
  def update
    render(:file => Rails.root.join('public', '403'), :formats => [:html], :status => 403, :layout => false)
  end

  # DELETE /invoices/1 or /invoices/1.json
  def destroy
    @invoice.destroy
    respond_to do |format|
      format.html { redirect_to invoices_url, notice: "Invoice was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_invoice
      @invoice = Invoice.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def invoice_params
      params.require(:invoice).permit(:round_id)
    end
end
