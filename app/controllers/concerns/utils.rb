module Utils
  extend ActiveSupport::Concern

  # included do
  #   before_action :recsys_has_seen, only: [:show]
  # end

  def is_admin
    unless current_user.has_role?(:admin)
      render(:file => Rails.root.join('public', '403'), :formats => [:html], :status => 403, :layout => false)
    end
  end
end
