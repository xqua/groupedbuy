class UsersController < ApplicationController
  include Utils

  before_action :authenticate_user!
  before_action :set_user, only: %i[ show edit update destroy make_admin validate]
  before_action :is_admin, only: %i[ index make_admin validate reset_validation]
  before_action :is_self, only: %i[ show edit update destroy ]

  # GET /users or /users.json
  def index
    @users = User.order('id DESC').all
  end

  # GET /users/1 or /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users or /users.json
  def create
    @user = User.new(user_params)
    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: "User was successfully created." }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1 or /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: "User was successfully updated." }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1 or /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: "User was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  def validate
    unless @user.validated
      @user.validated = true
      @user.validated_by = current_user.first_name + " " + current_user.last_name
      @user.validated_at = Time.now
    else
      @user.validated = false
      @user.validated_by = current_user.first_name + " " + current_user.last_name
      @user.validated_at = Time.now
    end
    if @user.save
      respond_to do |format|
        format.html { redirect_to request.referrer, notice: I18n.t(:validated_status_changed) }
      end
    end
  end

  def reset_validation
    for user in User.all
      user.validated = false
      user.validated_by = ""
      user.validated_at = nil
      user.save
    end
  end

  def make_admin
    unless @user.has_role? :admin
      @user.add_role :admin
    else
      @user.remove_role :admin
    end
    respond_to do |format|
      format.html { redirect_to request.referrer, notice: I18n.t(:admin_status_changed) }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    def is_self
      unless current_user.email == @user.email or current_user.has_role?(:admin)
        render(:file => Rails.root.join('public', '403'), :formats => [:html], :status => 403, :layout => false)
      end
    end

    # Only allow a list of trusted parameters through.
    def user_params
      params.require(:user).permit(:email, :first_name, :last_name, :phone)
    end
end
