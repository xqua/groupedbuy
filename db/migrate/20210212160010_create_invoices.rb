class CreateInvoices < ActiveRecord::Migration[6.1]
  def change
    create_table :invoices do |t|
      t.belongs_to :user
      t.belongs_to :round
      t.integer :total
      t.text :csv
      t.timestamps
    end
  end
end
