class CreateProducts < ActiveRecord::Migration[6.1]
  def change
    create_table :products do |t|
      t.string :name
      t.belongs_to :provider
      t.float :price
      t.string :unit
      t.float :min_quantity
      t.belongs_to :category
      t.timestamps
    end
  end
end
