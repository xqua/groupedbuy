class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :phone
      t.boolean :validated
      t.datetime :validated_at
      t.string :validated_by
      t.timestamps
    end
  end
end
