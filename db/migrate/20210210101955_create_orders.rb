class CreateOrders < ActiveRecord::Migration[6.1]
  def change
    create_table :orders do |t|
      t.belongs_to :user
      t.belongs_to :round
      t.boolean :validated, default: false
      t.boolean :submitted, default: false
      t.timestamps
    end
  end
end
