class CreateConditionings < ActiveRecord::Migration[6.1]
  def change
    create_table :conditionings do |t|
      t.string :cat_nb
      t.belongs_to :product
      t.string :unit
      t.float :quantity
      t.float :price
      t.timestamps
    end
  end
end
